import logging
import os
import os.path
from threading import Lock

_loggers_per_name = {}
_lock = Lock()


def get_logger(name: str) -> logging.Logger:
    global _loggers_per_name, _lock
    _lock.acquire()
    if name in _loggers_per_name:
        result = _loggers_per_name[name]
        _lock.release()
        return result

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('[%(levelname)s | %(asctime)s] %(message)s (%(module)s: %(funcName)s)',
                                  '%d/%m/%Y %H:%M:%S %Z')

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    if not os.path.isdir('logs'):
        os.mkdir('logs')
    file_handler = logging.FileHandler(os.path.join('logs', f'{name}.log'), encoding = 'utf-8')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    _loggers_per_name[name] = logger
    _lock.release()
    return logger
