import json
import os.path
import Utility
from datetime import date
from Logger import get_logger
from openpyxl import Workbook
from openpyxl.styles import NamedStyle, Font, PatternFill, Color, Alignment
from openpyxl.worksheet.worksheet import Worksheet
from requests import Session


SPREADSHEET_DIR = os.path.abspath(os.path.join('..', 'data', 'spreadsheet'))
JSON_DATA_DIR = os.path.abspath(os.path.join('..', 'data'))
_logger = get_logger('MetaMAL.SpreadsheetExporter')


def generate_spreadsheet(season: str, year: int):
    global _logger
    try:
        # Get statistics
        with open(os.path.join(JSON_DATA_DIR, f'{year}-{season}-full.json')) as data_file:
            stats = json.load(data_file)
        stats = _reshape_statistics(stats)
        season = season.capitalize()
        _logger.debug('JSON statistics reshaped successfully')

        # Generate views for each anime category
        wb = Workbook()
        _set_custom_styles(wb)
        _generate_anime_views(stats['tv_new'], wb, 'TV (New)', season, year)
        _logger.debug('"TV (New)" sheet generated')
        _generate_anime_views(stats['tv_continuing'], wb, 'TV (Continuing)', season, year)
        _logger.debug('"TV (Continuing)" sheet generated')
        _generate_anime_views(stats['ona'], wb, 'ONA', season, year)
        _logger.debug('"ONA" sheet generated')
        _generate_anime_views(stats['ova'], wb, 'OVA', season, year)
        _logger.debug('"OVA" sheet generated')
        _generate_anime_views(stats['movie'], wb, 'Movie', season, year)
        _logger.debug('"Movie" sheet generated')
        _generate_anime_views(stats['special'], wb, 'Special', season, year)
        _logger.debug('"Special" sheet generated')

        # Generate views for characters and seiyuu
        _generate_characters_or_seiyuu_view(wb, stats['characters'], True, season, year)
        _logger.debug('"Characters" sheet generated')
        _generate_characters_or_seiyuu_view(wb, stats['seiyuu'], False, season, year)
        _logger.debug('"Seiyuu" sheet generated')

        # Save the spreadsheet
        export_path = os.path.abspath(os.path.join(SPREADSHEET_DIR, f'{year}-{season.lower()}.xlsx'))
        os.makedirs(os.path.dirname(export_path), exist_ok = True)
        wb.save(export_path)
        _logger.info(f'Spreadsheet for {season} {year} successfully exported to "{export_path}"')
    except Exception as exception:
        _logger.exception("It wasn't possible to generate and export the spreadsheet.", exception)


def _reshape_statistics(stats: dict) -> dict:
    top_score = stats['TopAnimeScore']
    result = {}

    for category, entries in top_score.items():
        category_items = {}
        for timestamp, daily_entries in entries.items():
            for anime in daily_entries:
                anime_values = category_items.get(anime['Title'], [])
                anime_values.append(anime)
                category_items[anime['Title']] = anime_values
            _set_ranking(category_items)
        result[category] = category_items

    top_characters = stats['TopCharacters']
    reshaped_characters = []
    rank = 1
    for delta, entries in top_characters:
        for entry in entries:
            entry['FavouritesRank'] = rank
            entry['FavouritesChange'] = delta
            reshaped_characters.append(entry)
        rank += len(entries)
    result['characters'] = reshaped_characters

    top_seiyuu = stats['TopSeiyuu']
    reshaped_seiyuu = []
    rank = 1
    for delta, entries in top_seiyuu:
        for entry in entries:
            entry['FavouritesRank'] = rank
            entry['FavouritesChange'] = delta
            reshaped_seiyuu.append(entry)
        rank += len(entries)
    result['seiyuu'] = reshaped_seiyuu
    return result


def _set_ranking(stats: dict):
    titles_per_score = {}
    titles_per_members = {}

    for title, entries in stats.items():
        last_entry = entries[-1]
        score = last_entry['Score']
        if score is not None:
            temp = titles_per_score.get(score, [])
            temp.append(title)
            titles_per_score[score] = temp

        members = last_entry['Members']
        if score is not None:
            temp = titles_per_members.get(members, [])
            temp.append(title)
            titles_per_members[members] = temp

    score_descending = tuple(sorted(titles_per_score.keys(), reverse = True))
    rank = 1
    for score in score_descending:
        for anime in titles_per_score[score]:
            new_stats = stats[anime][-1]
            new_stats['ScoreRank'] = rank
            stats[anime][-1] = new_stats
        rank += len(titles_per_score[score])

    members_descending = tuple(sorted(titles_per_members.keys(), reverse = True))
    rank = 1
    for members in members_descending:
        for anime in titles_per_members[members]:
            new_stats = stats[anime][-1]
            new_stats['MembersRank'] = rank
            stats[anime][-1] = new_stats
        rank += len(titles_per_members[members])


def _set_custom_styles(wb: Workbook):
    title_style = NamedStyle('title', font = Font(name = 'Calibri', bold = True, size = 15, color = 'FFFFFF'),
                             fill = PatternFill(fill_type = 'solid', fgColor = Color('673AB7')))
    wb.add_named_style(title_style)

    legend_style = NamedStyle('legend', font = Font(name = 'Calibri', size = 14, color = 'FFFFFF'),
                              fill = PatternFill(fill_type = 'solid', fgColor = Color('7E57C2')),
                              alignment = Alignment(horizontal = 'center'))
    wb.add_named_style(legend_style)

    data_dark_style = NamedStyle('data dark', font = Font(name = 'Calibri', size = 13),
                                 fill = PatternFill(fill_type = 'solid', fgColor = Color('CFD8DC')),
                                 alignment = Alignment(horizontal = 'center'))
    wb.add_named_style(data_dark_style)

    data_light_style = NamedStyle('data light', font = Font(name = 'Calibri', size = 13),
                                  fill = PatternFill(fill_type = 'solid', fgColor = Color('ECEFF1')),
                                  alignment = Alignment(horizontal = 'center'))
    wb.add_named_style(data_light_style)


def _generate_anime_views(stats: dict, wb: Workbook, category: str, season: str, year: int):
    # Setup new sheet
    if category == 'TV (New)':
        ws = wb.worksheets[0]
        ws.title = category
        ws.sheet_properties.tabColor = '009688'
    elif category == 'TV (Continuing)':
        ws = wb.create_sheet(category)
        ws.sheet_properties.tabColor = '03A9F4'
    elif category == 'ONA':
        ws = wb.create_sheet(category)
        ws.sheet_properties.tabColor = 'E91E63'
    elif category == 'OVA':
        ws = wb.create_sheet(category)
        ws.sheet_properties.tabColor = '4CAF50'
    elif category == 'Movie':
        ws = wb.create_sheet(category)
        ws.sheet_properties.tabColor = 'FF9800'
    elif category == 'Special':
        ws = wb.create_sheet(category)
        ws.sheet_properties.tabColor = '3F51B5'
    else:
        raise ValueError(f'"{category}" is not a valid category value')

    # Add header
    _add_header(ws, category, season, year)

    # Set columns' width
    ws.column_dimensions['A'].width = 15
    ws.column_dimensions['B'].width = 15
    ws.column_dimensions['C'].width = 18
    ws.column_dimensions['D'].width = 20
    ws.column_dimensions['E'].width = 15
    ws.column_dimensions['F'].width = 15
    ws.column_dimensions['G'].width = 15

    # Add anime tables
    titles = tuple(sorted(stats.keys()))
    first_row = 4
    for title in titles:
        _add_anime_table(ws, first_row, stats[title])
        first_row += len(stats[title]) + 5


def _add_header(ws: Worksheet, category: str, season: str, year: int):
    # Merge top cells
    ws.merge_cells('A1:G1')

    # Set font and row dimension
    header = NamedStyle(f'header {category}',
                        font = Font(name = 'Calibri', bold = True, size = 18, color = 'FFFFFF'),
                        fill = PatternFill(fill_type = 'solid', fgColor = Color(ws.sheet_properties.tabColor.rgb)))
    ws.row_dimensions[1].height = 25
    ws['A1'].style = header

    # Print title and date
    today = date.today().strftime('%B %d, %Y')
    ws['A1'] = f'{category} • {season} {year} (generated on {today} by MetaMAL)'


def _add_anime_table(ws: Worksheet, first_row: int, anime_stats: list):
    # --------------------------------------------------------------------------------
    # Title
    # --------------------------------------------------------------------------------
    # Date | Score | Score rank | Delta score | Members | Members rank | Delta members
    # --------------------------------------------------------------------------------

    # Format and print title
    ws.merge_cells(start_row = first_row, end_row = first_row,
                   start_column = 1, end_column = 7)
    title_cell = ws.cell(first_row, 1)
    ws.row_dimensions[first_row].height = 20
    title_cell.style = 'title'
    title_cell.value = anime_stats[-1]['Title']

    # Format and print columns' legend
    cell = ws.cell(first_row + 1, 1, 'Date')
    cell.style = 'legend'

    cell = ws.cell(first_row + 1, 2, 'Members')
    cell.style = 'legend'

    cell = ws.cell(first_row + 1, 3, 'Members rank')
    cell.style = 'legend'

    cell = ws.cell(first_row + 1, 4, 'Members change')
    cell.style = 'legend'

    cell = ws.cell(first_row + 1, 5, 'Score')
    cell.style = 'legend'

    cell = ws.cell(first_row + 1, 6, 'Score rank')
    cell.style = 'legend'

    cell = ws.cell(first_row + 1, 7, 'Score change')
    cell.style = 'legend'

    # Print data for each day
    offset = 2

    for i in range(len(anime_stats)):
        daily_data = anime_stats[i]
        row_style = 'data light' if offset % 2 == 0 else 'data dark'

        # Date
        cell = ws.cell(first_row + offset, 1, daily_data['Timestamp'])
        cell.style = row_style

        # Members
        cell = ws.cell(first_row + offset, 2, daily_data['Members'])
        cell.style = row_style

        # Members rank
        cell = ws.cell(first_row + offset, 3,
                       f"#{daily_data['MembersRank']}" if 'MembersRank' in daily_data
                                                          and daily_data['MembersRank'] is not None else '')
        cell.style = row_style

        # Members change
        previous_members = anime_stats[i - 1]['Members'] if i > 0 else None
        delta = ''
        if previous_members is not None:
            delta = daily_data['Members'] - previous_members
            delta = f'+{delta}' if delta > 0 else f'{delta}'
        cell = ws.cell(first_row + offset, 4, delta)
        cell.style = row_style

        # Score
        cell = ws.cell(first_row + offset, 5, daily_data['Score'])
        cell.style = row_style

        # Score rank
        cell = ws.cell(first_row + offset, 6,
                       f"#{daily_data['ScoreRank']}" if 'ScoreRank' in daily_data
                                                        and daily_data['ScoreRank'] is not None else '')
        cell.style = row_style

        # Score change
        previous_members = anime_stats[i - 1]['Score'] if i > 0 else None
        delta = ''
        if previous_members is not None:
            delta = daily_data['Score'] - previous_members
            delta = f'+{round(delta, 2)}' if delta > 0 else f'{round(delta, 2)}'
        cell = ws.cell(first_row + offset, 7, delta)
        cell.style = row_style

        offset += 1


def _generate_characters_or_seiyuu_view(wb: Workbook, stats: list, is_character: bool, season: str, year: int):
    # ------------------------------------------------------
    # Name | Favourites rank | Favourites delta | From | To
    # ------------------------------------------------------

    # Setup new sheet
    if is_character:
        ws = wb.create_sheet('Characters')
        ws.title = 'Characters'
        ws.sheet_properties.tabColor = '795548'
        category = 'Characters'
    else:
        ws = wb.create_sheet('Seiyuu')
        ws.title = 'Seiyuu'
        ws.sheet_properties.tabColor = '607D8B'
        category = 'Seiyuu'

    # Set columns' width
    ws.column_dimensions['A'].width = 30
    ws.column_dimensions['B'].width = 20
    ws.column_dimensions['C'].width = 20
    ws.column_dimensions['D'].width = 15
    ws.column_dimensions['E'].width = 15

    # Merge top cells
    ws.merge_cells('A1:H1')

    # Set font and row dimension
    header = NamedStyle(f'header {category}',
                        font = Font(name = 'Calibri', bold = True, size = 18, color = 'FFFFFF'),
                        fill = PatternFill(fill_type = 'solid', fgColor = Color(ws.sheet_properties.tabColor.rgb)))
    ws.row_dimensions[1].height = 25
    ws['A1'].style = header

    # Print title and date
    today = date.today().strftime('%B %d, %Y')
    ws['A1'] = f'{category} • {season} {year} (generated on {today} by MetaMAL)'

    # Format and print columns' legend
    cell = ws.cell(4, 1, 'Name')
    cell.style = 'legend'

    cell = ws.cell(4, 2, 'Favourites rank')
    cell.style = 'legend'

    cell = ws.cell(4, 3, 'Favourites change')
    cell.style = 'legend'

    cell = ws.cell(4, 4, 'From')
    cell.style = 'legend'

    cell = ws.cell(4, 5, 'To')
    cell.style = 'legend'

    # Print data for each character/seiyuu
    offset = 0

    for entity in stats:
        row_style = 'data light' if offset % 2 == 0 else 'data dark'

        # Name
        cell = ws.cell(5 + offset, 1, entity['Name'])
        cell.style = row_style

        # Favourites rank
        cell = ws.cell(5 + offset, 2,
                       f"#{entity['FavouritesRank']}" if 'FavouritesRank' in entity
                                                         and entity['FavouritesRank'] is not None else '')
        cell.style = row_style

        # Favourites change
        delta = f"+{entity['FavouritesChange']}" if entity['FavouritesChange'] > 0 else f"{entity['FavouritesChange']}"
        cell = ws.cell(5 + offset, 3, delta)
        cell.style = row_style

        # From
        cell = ws.cell(5 + offset, 4, entity['Members'] - entity['FavouritesChange'])
        cell.style = row_style

        # To
        cell = ws.cell(5 + offset, 5, entity['Members'])
        cell.style = row_style

        offset += 1


if __name__ == "__main__":
    session = Session()
    season, year = Utility.Season.get_current_season(session)
    session.close()
    generate_spreadsheet(season.value, year)
