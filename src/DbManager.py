import Backup
import Logger
import os
import os.path
import random
import requests
import time
from Crawler import Crawler
from datetime import datetime
from lxml import html
from sqlite3 import Connection, connect
from Utility import Season, Genre, download_page


DATABASE_PATH = os.path.abspath(os.path.join('..', 'database', 'Seasonal.sqlite'))
_logger = Logger.get_logger('MetaMAL.DbManager')


def write_to_database(crawler: Crawler):
    global _logger, DATABASE_PATH
    try:
        connection = get_new_connection()
        _create_anime_name_table_if_missing(connection)
        _create_character_name_table_if_missing(connection)
        _create_seiyuu_name_table_if_missing(connection)

        _insert_category_in_database(connection, crawler.tv_new, crawler.year, crawler.season, 'tv_new')
        _insert_category_in_database(connection, crawler.tv_continuing, crawler.year, crawler.season, 'tv_continuing')
        _insert_category_in_database(connection, crawler.ona, crawler.year, crawler.season, 'ona')
        _insert_category_in_database(connection, crawler.ova, crawler.year, crawler.season, 'ova')
        _insert_category_in_database(connection, crawler.movie, crawler.year, crawler.season, 'movie')
        _insert_category_in_database(connection, crawler.special, crawler.year, crawler.season, 'special')

        _logger.debug('Committing changes to database...')
        connection.commit()
        _logger.info('Changes committed to database.')
        connection.close()
        _logger.debug('Connection closed.')
    except Exception as exception:
        _logger.critical('Critical error during database writing', exception, exc_info = True)


def get_new_connection(read_only = False) -> Connection:
    global DATABASE_PATH, _logger
    _logger.debug(f'Connecting to {DATABASE_PATH}...')
    directory = os.path.dirname(DATABASE_PATH)
    os.makedirs(directory, exist_ok = True)

    if read_only:
        uri = f'file:{DATABASE_PATH}?mode=ro'
    else:
        uri = f'file:{DATABASE_PATH}?mode=rwc'
    connection = connect(uri, timeout = 30, uri = True)
    _logger.debug(f'Successfully connected to {DATABASE_PATH}.')
    return connection


def _insert_category_in_database(connection: Connection, anime_list: list, year: int, season: Season, category: str):
    global _logger
    if year > 0:
        table_name = f'{year}_{season.value}_{category}'
    else:
        table_name = f'unknown_{season.value}_{category}'
    timestamp = datetime.utcnow().strftime('%Y-%m-%d')

    # Insert anime score and members
    _create_anime_table_if_missing(connection, table_name)
    data = [(anime.id, anime.score, anime.members, timestamp) for anime in anime_list]
    _logger.debug(f'Generated data for {len(data)} rows in table "{table_name}".')
    connection.executemany(f'INSERT INTO "{table_name}" ("AnimeId", "Score", "Members", "Timestamp")'
                           'VALUES (?, ?, ?, ?)', data)
    _logger.debug(f'{len(data)} rows inserted in table "{table_name}".')

    # Add or update anime title and genres per ID
    data = [(anime.id, anime.title, Genre.encode_to_string(anime.genres)) for anime in anime_list]
    _logger.debug(f'Generated data for {len(data)} rows in table "id_title".')
    connection.executemany('REPLACE INTO "id_title" ("AnimeId", "Title", "Genres") VALUES (?, ?, ?)', data)
    _logger.debug(f'{len(data)} rows replaced in table "id_title".')

    # Insert character favourites
    table_name = f'{year}_{season.value}_characters'
    _create_character_favourites_table_if_missing(connection, table_name)
    for anime in anime_list:
        data = [(character.id, character.favourites, anime.id, timestamp) for character in anime.characters]
        if len(data) > 0:
            _logger.debug(f'Generated data for {len(data)} rows in table "{table_name}".')
            connection.executemany(
                f'INSERT INTO "{table_name}" ("CharacterId", "Favourites","AnimeId", "Timestamp") VALUES (?, ?, ?, ?)',
                data)
            _logger.debug(f'{len(data)} rows replaced in table "{table_name}".')

        # Add or update character name per ID
        data = [(character.id, character.name) for character in anime.characters]
        if len(data) > 0:
            _logger.debug(f'Generated data for {len(data)} rows in table "id_character".')
            connection.executemany('REPLACE INTO "id_character" ("CharacterId", "Name") VALUES (?, ?)', data)
            _logger.debug(f'{len(data)} rows replaced in table "id_character".')

    # Insert seiyuu favourites
    table_name = f'{year}_{season.value}_seiyuu'
    _create_seiyuu_favourites_table_if_missing(connection, table_name)
    for anime in anime_list:
        data = [(seiyuu.id, seiyuu.favourites, timestamp) for seiyuu in anime.seiyuu]
        if len(data) > 0:
            _logger.debug(f'Generated data for {len(data)} rows in table "{table_name}".')
            connection.executemany(
                f'INSERT INTO "{table_name}" ("PersonId", "Favourites", "Timestamp") VALUES (?, ?, ?)',
                data)
            _logger.debug(f'{len(data)} rows replaced in table "{table_name}".')

        # Add or update seiyuu name per ID
        data = [(seiyuu.id, seiyuu.name) for seiyuu in anime.seiyuu]
        if len(data) > 0:
            _logger.debug(f'Generated data for {len(data)} rows in table "id_seiyuu".')
            connection.executemany('REPLACE INTO "id_seiyuu" ("PersonId", "Name") VALUES (?, ?)', data)
            _logger.debug(f'{len(data)} rows replaced in table "id_seiyuu".')


def _create_anime_table_if_missing(connection: Connection, table_name: str):
    global _logger
    query = f'''
create table if not exists "{table_name}"
(
    Id INTEGER
        constraint "{table_name + '_pk'}"
            primary key autoincrement,
    AnimeId INTEGER not null,
    Score REAL,
    Members INTEGER,
    Timestamp TEXT not null
);'''

    connection.execute(query)
    _logger.debug(f'Table "{table_name}" created (if needed).')


def _create_anime_name_table_if_missing(connection: Connection):
    global _logger
    script = '''
create table if not exists "id_title"
(
    AnimeId INTEGER not null
        constraint "id_title_pk"
            primary key,
    Title TEXT not null,
    Genres TEXT
);

create unique index if not exists "id_title_AnimeId_uindex"
    on id_title (AnimeId);'''

    connection.executescript(script)
    _logger.debug('Table "id_title" created (if needed).')


def _create_character_name_table_if_missing(connection: Connection):
    global _logger
    script = '''
create table if not exists "id_character"
(
    CharacterId INTEGER not null
        constraint "id_character_pk"
            primary key,
    Name TEXT not null
);

create unique index if not exists "id_character_CharacterId_uindex"
    on id_character (CharacterId);'''

    connection.executescript(script)
    _logger.debug('Table "id_character" created (if needed).')


def _create_seiyuu_name_table_if_missing(connection: Connection):
    global _logger
    script = '''
create table if not exists "id_seiyuu"
(
    PersonId INTEGER not null
        constraint "id_seiyuu_pk"
            primary key,
    Name TEXT not null
);

create unique index if not exists "id_seiyuu_PersonId_uindex"
    on id_seiyuu (PersonId);'''

    connection.executescript(script)
    _logger.debug('Table "id_seiyuu" created (if needed).')


def _create_character_favourites_table_if_missing(connection: Connection, table_name: str):
    global _logger
    query = f'''
create table if not exists "{table_name}"
(
    Id INTEGER
        constraint "{table_name + '_pk'}"
            primary key autoincrement,
    CharacterId INTEGER not null,
    Favourites INTEGER default 0 not null,
    AnimeId INTEGER not null,
    Timestamp TEXT not null
);'''

    connection.execute(query)
    _logger.debug(f'Table "{table_name}" created (if needed).')


def _create_seiyuu_favourites_table_if_missing(connection: Connection, table_name: str):
    global _logger
    query = f'''
create table if not exists "{table_name}"
(
    Id INTEGER
        constraint "{table_name + '_pk'}"
            primary key autoincrement,
    PersonId INTEGER not null,
    Favourites INTEGER default 0 not null,
    Timestamp TEXT not null
);'''

    connection.execute(query)
    _logger.debug(f'Table "{table_name}" created (if needed).')


def get_anime_per_timestamp(connection: Connection, timestamp: str, year: int, season: Season, category: str) -> list:
    global _logger
    if year > 0:
        table_name = f'{year}_{season.value}_{category}'
    else:
        table_name = f'unknown_{season.value}_{category}'

    cursor = connection.execute('SELECT "AnimeId", "Score", "Members", "Timestamp"'
                                f'FROM "{table_name}" WHERE "Timestamp" = ?', (timestamp,))

    results = [AnimeData(row) for row in cursor.fetchall()]
    cursor.close()
    _logger.debug(f'Found {len(results)} series for {timestamp} in "{table_name}".')
    return results


def get_anime_per_season(connection: Connection, year: int, season: Season, category: str) -> list:
    global _logger
    if year > 0:
        table_name = f'{year}_{season.value}_{category}'
    else:
        table_name = f'unknown_{season.value}_{category}'

    cursor = connection.execute('SELECT "AnimeId", "Score", "Members", "Timestamp"'
                                f'FROM "{table_name}" ORDER BY DATE(Timestamp)')

    results = [AnimeData(row) for row in cursor.fetchall()]
    cursor.close()
    _logger.debug(f'Found {len(results)} series for {season} {year} in "{table_name}".')
    return results


def get_genres_per_anime(connection: Connection, anime_id: int) -> tuple:
    global _logger
    cursor = connection.execute('SELECT "Genres" FROM "id_title" WHERE AnimeId=?', (anime_id,))
    result = cursor.fetchone()
    cursor.close()

    if len(result) != 1:
        _logger.warning(f'Found {len(result)} genres values for Anime ID {anime_id} instead of 1.')
        return ()
    return Genre.decode_from_string(result[0])


def get_characters_per_season(connection: Connection, year: int, season: Season) -> list:
    global _logger
    if year > 0:
        table_name = f'{year}_{season.value}_characters'
    else:
        table_name = f'unknown_{season.value}_characters'

    cursor = connection.execute(f'SELECT "CharacterId", "Favourites", "Timestamp" FROM "{table_name}"')

    results = [EntityData(row, True) for row in cursor.fetchall()]
    cursor.close()
    _logger.debug(f'Found {len(results)} characters for {season} {year}.')
    return results


def get_seiyuu_per_season(connection: Connection, year: int, season: Season) -> list:
    global _logger
    if year > 0:
        table_name = f'{year}_{season.value}_seiyuu'
    else:
        table_name = f'unknown_{season.value}_seiyuu'

    cursor = connection.execute(f'SELECT "PersonId", "Favourites", "Timestamp" FROM "{table_name}"')

    results = [EntityData(row, False) for row in cursor.fetchall()]
    cursor.close()
    _logger.debug(f'Found {len(results)} seiyuu for {season} {year}.')
    return results


def get_anime_title(connection: Connection, anime_id: int) -> str:
    cursor = connection.execute('SELECT "Title" FROM "id_title" WHERE AnimeId=?', (anime_id,))
    result = cursor.fetchone()[0]
    cursor.close()
    _logger.debug(f'Anime ID {anime_id} = {result}.')
    return result


def get_character_name(connection: Connection, character_id: int) -> str:
    cursor = connection.execute('SELECT "Name" FROM "id_character" WHERE CharacterId=?', (character_id,))
    result = cursor.fetchone()[0]
    cursor.close()
    _logger.debug(f'Character ID {character_id} = {result}.')
    return result


def get_seiyuu_name(connection: Connection, person_id: int) -> str:
    cursor = connection.execute('SELECT "Name" FROM "id_seiyuu" WHERE PersonId=?', (person_id,))
    result = cursor.fetchone()[0]
    cursor.close()
    _logger.debug(f'Person ID {person_id} = {result}.')
    return result


def remove_duplicates(session, year = None, season = None):
    global _logger
    if year is None or season is None:
        season, year = Season.get_current_season(session)
    connection = get_new_connection(False)
    remove_duplicated_characters(connection, year, season)
    remove_duplicated_seiyuu(connection, year, season)
    connection.commit()
    connection.close()
    _logger.debug('Removed all duplicates.')


def remove_duplicated_characters(connection: Connection, year: int, season: Season):
    global _logger
    if year > 0:
        table_name = f'{year}_{season.value}_characters'
    else:
        table_name = f'unknown_{season.value}_characters'

    entries = set()
    cursor = connection.execute(f'SELECT "Id", "CharacterId", "Timestamp" FROM "{table_name}"')
    results = cursor.fetchall()
    cursor.close()

    duplicates = []
    for row in results:
        fingerprint = f'{row[1]}#{row[2]}'
        if fingerprint not in entries:
            entries.add(fingerprint)
        else:
            duplicates.append((row[0],))

    if len(duplicates) > 0:
        _logger.info(f'Found {len(duplicates)} duplicated rows in {table_name}.')
        cursor = connection.executemany(f'DELETE FROM "{table_name}" WHERE Id=?', duplicates)
        cursor.close()
    else:
        _logger.debug(f'No duplicated rows found in {table_name}')


def remove_duplicated_seiyuu(connection: Connection, year: int, season: Season):
    global _logger
    if year > 0:
        table_name = f'{year}_{season.value}_seiyuu'
    else:
        table_name = f'unknown_{season.value}_seiyuu'

    entries = set()
    cursor = connection.execute(f'SELECT "Id", "PersonId", "Timestamp" FROM "{table_name}"')
    results = cursor.fetchall()
    cursor.close()

    duplicates = []
    for row in results:
        fingerprint = f'{row[1]}#{row[2]}'
        if fingerprint not in entries:
            entries.add(fingerprint)
        else:
            duplicates.append((row[0],))

    if len(duplicates) > 0:
        _logger.info(f'Found {len(duplicates)} duplicated rows in {table_name}.')
        cursor = connection.executemany(f'DELETE FROM "{table_name}" WHERE Id=?', duplicates)
        cursor.close()
    else:
        _logger.debug(f'No duplicated rows found in {table_name}')


def get_first_entry_per_season(connection: Connection, year: int, season: Season) -> str:
    table_name = f'{year}_{season.value}_tv_new'
    cursor = connection.execute(f'SELECT "Timestamp" FROM "{table_name}" ORDER BY "Timestamp" LIMIT 1')
    result = cursor.fetchone()[0]
    cursor.close()
    _logger.debug(f'First timestamp in {table_name}: {result}.')
    return result


def get_last_entry_per_season(connection: Connection, year: int, season: Season) -> str:
    table_name = f'{year}_{season.value}_tv_new'
    cursor = connection.execute(f'SELECT "Timestamp" FROM "{table_name}" ORDER BY "Timestamp" DESC LIMIT 1')
    result = cursor.fetchone()[0]
    cursor.close()
    _logger.debug(f'Last timestamp in {table_name}: {result}.')
    return result


def compress_database(connection: Connection):
    cursor = connection.execute('VACUUM')
    cursor.close()
    _logger.debug('Database compressed successfully.')


class AnimeData:
    def __init__(self, data: tuple):
        self.id = data[0]
        self.score = data[1]
        self.members = data[2]
        self.timestamp = data[3]
        self.title = None

    def __str__(self) -> str:
        return f'[{self.timestamp}] {self.id} → Score: {self.score}; Members: {self.members}; Title: {self.title}'

    def __dict__(self) -> dict:
        return {
            "Id"       : self.id,
            "Title"    : self.title,
            "Score"    : self.score,
            "Members"  : self.members,
            "Timestamp": self.timestamp
            }

    def load_title(self, connection):
        if self.title is None:
            self.title = get_anime_title(connection, self.id)


class EntityData:
    def __init__(self, data: tuple, is_character: bool):
        self.id = data[0]
        self.members = data[1]
        self.timestamp = data[2]
        self._is_character = is_character
        self.name = None

    def __str__(self) -> str:
        return f'[{self.timestamp}] {self.id} → Members: {self.members}; Character: {self._is_character}; Name: {self.name}'

    def __dict__(self) -> dict:
        return {
            "Id"       : self.id,
            "Name"     : self.name,
            "Members"  : self.members,
            "Timestamp": self.timestamp
            }

    def load_name(self, connection):
        if self.name is None:
            if self._is_character:
                self.name = get_character_name(connection, self.id)
            else:
                self.name = get_seiyuu_name(connection, self.id)

    def save_picture(self, session: requests.Session):
        url = '???'
        try:
            if self._is_character:
                url = f'https://myanimelist.net/character/{self.id}'
            else:
                url = f'https://myanimelist.net/people/{self.id}'

            time.sleep(random.uniform(2.0, 4.0))  # Avoid HTTP 429 Error: Too Many Requests
            page = download_page(url, session, False)
            parser = html.HTMLParser(encoding = 'utf-8')
            tree = html.document_fromstring(page, parser = parser)

            frames = tree.find_class('borderClass')
            picture = frames[0].getchildren()[0]
            picture = picture.getchildren()[0]
            picture = picture.getchildren()
            if len(picture) == 0:
                return
            picture_url = picture[0].attrib['data-src']  # Lazy-loaded image
            picture_bytes = download_page(picture_url, session, False)
            self._store_picture(picture_bytes)
        except Exception as exception:
            global _logger
            _logger.exception(f'Could not fetch entity picture "{url}".', exception)

    def _store_picture(self, picture_bytes: bytes):
        if self._is_character:
            path = os.path.abspath(os.path.join('..', 'pictures', 'characters', f'{self.id}'))
        else:
            path = os.path.abspath(os.path.join('..', 'pictures', 'seiyuu', f'{self.id}'))
        os.makedirs(os.path.dirname(path), exist_ok = True)

        extension, wrong_extensions = self._get_real_image_type(picture_bytes)
        for wrong_extension in wrong_extensions:
            old_file = f'{path}.{wrong_extension}'
            if os.path.isfile(old_file):
                os.remove(old_file)

        path = f'{path}.{extension}'
        with open(path, 'wb') as file:
            file.write(picture_bytes)

    def _get_real_image_type(self, picture_bytes: bytes) -> tuple:
        if picture_bytes.startswith(b'\xff\xd8\xff'):
            return 'jpg', ('png', 'gif', 'webp')
        elif picture_bytes.startswith(b'\x89PNG\r\n\x1a\n'):
            return 'png', ('jpg', 'gif', 'webp')
        elif picture_bytes.startswith((b'GIF87a', b'GIF89a')):
            return 'gif', ('png', 'jpg', 'webp')
        elif picture_bytes.startswith(b'RIFF') and len(picture_bytes) > 12 and picture_bytes[8:12] == b'WEBP':
            return 'webp', ('png', 'jpg', 'gif')
        else:
            global _logger
            _logger.warning(f'Unable to identify image type for {"character" if self._is_character else "person"} ID {self.id}.')
            return 'jpg', ('png', 'gif', 'webp')
