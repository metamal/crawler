import Logger
import random
import time
from Person import Person
from lxml import html
from lxml.html import HtmlElement
from requests import Session
from Utility import Genre, sanitize_string, download_page


class Anime:
    def __init__(self, div_tree: HtmlElement, session: Session):
        self.id = None
        self.title = None
        self.score = None
        self.members = None
        self.genres = []
        self.characters = []
        self.seiyuu = []
        self._logger = Logger.get_logger('MetaMAL.Anime')
        self._analyze_entry(div_tree, session)


    def __str__(self) -> str:
        return f'[{self.id}] {self.title} → Score: {self.score}; Members: {self.members}'


    def _analyze_entry(self, tree: HtmlElement, session: Session):
        try:
            self._analyze_title(tree)
            self._analyze_score(tree)
            self._analyze_members(tree)
            self._analyze_genres(tree)
            time.sleep(random.uniform(0.1, 1.0))  # Avoid HTTP 429 Error: Too Many Requests
            self._fetch_favourite_characters(session)
            self._logger.debug(f'Anime analyzed: {str(self)}')
        except Exception as exception:
            self._logger.exception(f'Unable to analyze entry ({str(self)}).', exception)


    def _analyze_title(self, tree: HtmlElement):
        title_element = tree.find_class('link-title')[0]
        self.title = sanitize_string(title_element.text_content())
        # URL example: https://myanimelist.net/anime/10357/Jinrui_wa_Suitai_Shimashita
        url = sanitize_string(title_element.attrib['href'])
        self.id = int(url.split('/')[4])


    def _analyze_score(self, tree: HtmlElement):
        score_element = tree.find_class('score')[0]
        score_as_text = sanitize_string(score_element.text_content()).replace(',', '.')
        if len(score_as_text) == 0:
            score_as_text = '0.0'
        if score_as_text != 'N/A':
            self.score = float(score_as_text)


    def _analyze_members(self, tree: HtmlElement):
        members_element = tree.find_class('member')[0]
        members_str = sanitize_string(members_element.text_content()).replace(',', '')

        multiplier = 1
        if members_str.endswith('K'):
            multiplier = 1_000
            members_str = members_str[:-1]
        elif members_str.endswith('M'):
            multiplier = 1_000_000
            members_str = members_str[:-1]
        elif len(members_str) == 0:
            members_str = '0'

        self.members = round(float(members_str) * multiplier)


    def _analyze_genres(self, tree: HtmlElement):
        genres = tree.find_class('genre')
        self.genres = [
            Genre.get_genre_from_string(
                    sanitize_string(
                            div.text_content()
                    )
            ) for div in genres]


    def _fetch_favourite_characters(self, session: Session):
        try:
            url = f'https://myanimelist.net/anime/{self.id}'
            raw_page = download_page(url, session, mobile_user_agent = True)
            parser = html.HTMLParser(encoding = 'utf-8')
            tree = html.document_fromstring(raw_page, parser = parser)
            self._analyze_favourite_characters_and_seiyuu(tree)
        except Exception as exception:
            self._logger.exception('Unable to download character page.', exception)


    def _analyze_favourite_characters_and_seiyuu(self, tree: HtmlElement):
        characters_divs = tree.find_class('slider anime-detail-actors')
        characters = [couple.find_class('slider-btn js-ajax-loading')[0]
                      for couple in characters_divs]
        seiyuu = [couple.find_class('slider-btn js-ajax-loading')[1]
                  for couple in characters_divs if len(couple.find_class('slider-btn js-ajax-loading')) == 2]

        self.characters = [Person(character) for character in characters]
        self.seiyuu = [Person(person) for person in seiyuu]
