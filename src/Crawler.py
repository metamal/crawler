import DataAnalyzer
import DbManager
import Logger
import time
from Anime import Anime
from datetime import datetime, timedelta
from lxml import html
from lxml.html import HtmlElement
from requests import Session
from Utility import Season, sanitize_string, download_page


class Crawler:
    def __init__(self):
        """
        Create a new Crawler without starting it.
        A Crawler can fetch the seasonal anime from MAL and analyze them.
        """
        self.season = None
        self.year = -1
        self.count = 0
        self.tv_new = []
        self.tv_continuing = []
        self.ona = []
        self.ova = []
        self.movie = []
        self.special = []
        self._logger = Logger.get_logger('MetaMAL.Crawler')
        self._logger.debug('New Crawler initialized.')

    def fetch_current_seasonal_anime(self) -> tuple:
        """
        Fetch a fresh copy of the current seasonal anime page from MyAnimeList and analyze each series.
        """
        try:
            session = Session()
            season, year = Season.get_current_season(session)
            self._logger.info(f'Analyzing anime for {season.value.capitalize()} {year}')
            self.fetch_seasonal_anime(season, year, session)
            return season, year
        except Exception as exception:
            self._logger.critical('Unable to fetch remote file.', exception, exc_info = True)

    def fetch_seasonal_anime(self, season: Season, year: int, session = None):
        """
        Fetch a fresh copy of the past or future seasonal anime page from MyAnimeList and analyze each series.
        :param season: Airing season for the series.
        :param year: Airing year for the series.
        :param session: Optional instance of requests.Session
        """
        try:
            if season is not Season.LATER:
                url = f'https://myanimelist.net/anime/season/{year}/{season.value}'
            else:
                url = 'https://myanimelist.net/anime/season/later'

            if session is None:
                session = Session()

            page = download_page(url, session, mobile_user_agent = False)
            self._analyze_seasonal_anime_page(page, session)
            session.close()
            self._logger.info(f'{self.count} anime analyzed successfully.')
        except Exception as exception:
            self._logger.critical('Unable to fetch remote file.', exception, exc_info = True)

    def _analyze_seasonal_anime_page(self, html_page: bytes, session: Session):
        """
        [This function should only be used for testing]
        Analyze an already fetched seasonal anime HTML page.
        :param html_page: HTML page provided as a byte stream.
        """
        self._logger.debug('Analyzing a raw HTML page.')
        parser = html.HTMLParser(encoding = 'utf-8')
        tree = html.document_fromstring(html_page, parser = parser)
        self._analyze_raw_page(tree, session)

    def _analyze_raw_page(self, tree: HtmlElement, session: Session):
        try:
            self._extract_season_name(tree)
            self._extract_categories(tree, session)
        except Exception as exception:
            self._logger.critical('Unable to analyze the HTML tree.', exception, exc_info = True)

    def _extract_season_name(self, tree: HtmlElement):
        on_elements = [e for e in tree.find_class('on') if e.tag == 'a']
        if len(on_elements) > 0:
            season_and_year = on_elements[0].text_content().strip().upper()
            season_and_year = season_and_year.split(' ')
            if len(season_and_year) > 2 or len(season_and_year) <= 0:
                self._logger.error(f'Found {len(season_and_year)} strings in "season_and_year" instead of 2 or 1.')
                self.season = Season.UNKNOWN
                self.year = -1
            elif season_and_year[0] == 'LATER':
                self.season = Season.LATER
                self.year = -2
            else:
                self.season = Season[season_and_year[0]]
                self.year = int(season_and_year[1])

        if len(on_elements) > 1:
            self._logger.warning(f'Found {len(on_elements)} "a" tags instead of 1.')

    def _extract_categories(self, tree: HtmlElement, session: Session):
        # There are 6 categories:
        # 1- TV (New)                 4- OVA
        # 2- TV (Continuing)          5- Movie
        # 3- ONA                      6- Special
        div_classes = 'seasonal-anime-list js-seasonal-anime-list'
        categories = [div for div in tree.find_class(div_classes)]

        if len(categories) != 6:
            self._logger.warning(f'Found {len(categories)} categories instead of 6.')
        self.count = 0

        self._logger.debug('Starting categories analysis.')
        for category in categories:
            try:
                self._analyze_category(category, session)
            except Exception as exception:
                self._logger.exception('Unable to analyze the category.', exception)

    def _analyze_category(self, category: HtmlElement, session: Session):
        category_name = sanitize_string(category.find_class('anime-header')[0].text_content())
        series_divs = category.find_class('seasonal-anime js-seasonal-anime')
        anime_list = [Anime(div, session) for div in series_divs]
        self._save_anime_list(category_name, anime_list)

    def _save_anime_list(self, category: str, anime: list):
        if category == 'TV (New)':
            self.tv_new = anime
        elif category == 'TV (Continuing)':
            self.tv_continuing = anime
        elif category == 'ONA':
            self.ona = anime
        elif category == 'OVA':
            self.ova = anime
        elif category == 'Movie':
            self.movie = anime
        elif category == 'Special':
            self.special = anime
        else:
            raise ValueError(f'Invalid argument "category": {category}')
        self.count += len(anime)
        self._logger.info(f'Category "{category}" analyzed successfully.')

    def run_once_per_day(self):
        while True:
            # Execute fetching approximately at 00:00 UTC.
            now = datetime.utcnow()
            tomorrow = now.date() + timedelta(days = 1)
            tomorrow = datetime.combine(tomorrow, datetime.min.time())
            delay = tomorrow - now
            self._logger.info(f'Sleeping for {delay.total_seconds()} seconds '
                              f'(≈ {str(delay.total_seconds() / 60 / 60)[:5]} hours).')
            time.sleep(delay.total_seconds())

            # Fetch statistics for the current season
            season, year = self.fetch_current_seasonal_anime()
            self.write_to_database()
            previous_season, previous_year, last_day_of_previous_season = DataAnalyzer.compute_website_data(season, year)

            # Fetch statistics for the previous season if it ended less than 15 days ago
            time_since_previous_season = now - datetime.strptime(last_day_of_previous_season, '%Y-%m-%d')
            if time_since_previous_season.days <= 20:
                previous_season_data = Crawler()
                previous_season_data.fetch_seasonal_anime(previous_season, previous_year)
                previous_season_data.write_to_database()
                DataAnalyzer.compute_website_data(previous_season, previous_year, True, True)

    def write_to_database(self):
        if self.count > 0:
            DbManager.write_to_database(self)
        else:
            self._logger.error("The crawler fetched 0 anime.")


if __name__ == "__main__":
    try:
        crawler = Crawler()
        crawler.run_once_per_day()
    except KeyboardInterrupt:
        Logger.get_logger('MetaMAL.Crawler').info('Crawler terminated by user.')
