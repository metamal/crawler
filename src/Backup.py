import boto3
import os
import os.path
from datetime import datetime
from Logger import get_logger


DATABASE_PATH = os.path.abspath(os.path.join('..', 'database', 'Seasonal.sqlite'))
_logger = get_logger('MetaMAL.Backup')


def backup_database():
    global _logger, DATABASE_PATH
    try:
        if not os.path.isfile(DATABASE_PATH):
            _logger.error(f'"{DATABASE_PATH}" does not exists.')
            return

        # Get S3 client
        archive = boto3.client('s3')

        # Upload database to S3 Glacier
        archive.upload_file(DATABASE_PATH, 'metamal-backup', f'MetaMAL database {_get_timestamp()}',
                            ExtraArgs = {'StorageClass': 'DEEP_ARCHIVE'})
        _logger.info('SQLite database successfully uploaded to S3 Glacier.')
    except Exception as exception:
        _logger.exception("It wasn't possible to backup SQLite database.", exception)


def _get_timestamp() -> str:
    return datetime.utcnow().strftime('%Y-%m-%d')


if __name__ == "__main__":
    backup_database()
