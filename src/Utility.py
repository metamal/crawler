import requests
from enum import Enum
from Logger import get_logger
from lxml import html

_logger = get_logger('MetaMAL.Utility')


def sanitize_string(string: str) -> str:
    string = string.strip()
    string = string.replace('\n', '')
    previous_length = len(string)
    while True:
        string = string.replace('  ', ' ')
        if len(string) == previous_length:
            break
        previous_length = len(string)
    return string


def download_page(url: str, session: requests.Session, mobile_user_agent: bool, timeout = 40.0, raise_for_status = True) -> bytes:
    global _logger
    _logger.debug(f'Downloading page "{url}", mobile: {mobile_user_agent}.')

    headers = {
        'Pragma'       : 'no-cache',
        'Cache-Control': 'no-cache'
        }

    if mobile_user_agent:
        headers['User-Agent'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'
    else:
        headers['User-Agent'] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:76.0) Gecko/20100101 Firefox/76.0'

    response = session.get(url, headers = headers, timeout = timeout)
    if raise_for_status:
        response.raise_for_status()
    _logger.debug(f'Page "{url}" successfully downloaded.')
    content = response.content
    response.close()
    return content


class Season(Enum):
    FALL = 'fall'
    SPRING = 'spring'
    SUMMER = 'summer'
    WINTER = 'winter'
    LATER = 'later'
    UNKNOWN = 'unknown'

    @staticmethod
    def get_current_season(session: requests.Session) -> tuple:
        page = download_page('https://myanimelist.net/anime/season', session, mobile_user_agent = False)
        parser = html.HTMLParser(encoding = 'utf-8')
        tree = html.document_fromstring(page, parser = parser)
        season_text = [e for e in tree.find_class('on') if e.tag == 'a'][0].text_content()
        season_text = sanitize_string(season_text).upper().split(' ')
        season = Season[season_text[0]]
        year = int(season_text[1])
        _logger.debug(f'Current season: {season_text}.')
        return season, year

    @staticmethod
    def get_next_season(session: requests.Session, season = None, year = -1):
        if season is None:
            season, year = Season.get_current_season(session)
        if season is Season.WINTER:
            return Season.SPRING, year
        elif season is Season.SPRING:
            return Season.SUMMER, year
        elif season is Season.SUMMER:
            return Season.FALL, year
        else:
            return Season.WINTER, year + 1

    @staticmethod
    def get_previous_season(session: requests.Session, season = None, year = -1):
        if season is None:
            season, year = Season.get_current_season(session)
        if season is Season.WINTER:
            return Season.FALL, year - 1
        elif season is Season.FALL:
            return Season.SUMMER, year
        elif season is Season.SUMMER:
            return Season.SPRING, year
        else:
            return Season.WINTER, year


class Genre(Enum):
    ACTION = 1
    ADVENTURE = 2
    CARS = 3
    COMEDY = 4
    AVANT_GARDE = 5  # Previously "Dementia"
    DEMONS = 6
    MYSTERY = 7
    DRAMA = 8
    ECCHI = 9
    FANTASY = 10
    GAME = 11
    HENTAI = 12
    HISTORICAL = 13
    HORROR = 14
    KIDS = 15
    MAGIC = 16  # Doesn't exist anymore
    MARTIAL_ARTS = 17
    MECHA = 18
    MUSIC = 19
    PARODY = 20
    SAMURAI = 21
    ROMANCE = 22
    SCHOOL = 23
    SCI_FI = 24
    SHOUJO = 25
    GIRLS_LOVE = 26  # Previously "Shoujo ai"
    SHOUNEN = 27
    BOYS_LOVE = 28  # Previously "Shounen ai"
    SPACE = 29
    SPORTS = 30
    SUPER_POWER = 31
    VAMPIRE = 32
    YAOI = 33  # Doesn't exist anymore
    YURI = 34  # Doesn't exist anymore
    HAREM = 35
    SLICE_OF_LIFE = 36
    SUPERNATURAL = 37
    MILITARY = 38
    POLICE = 39
    PSYCHOLOGICAL = 40
    SUSPENSE = 41  # Previously "Thriller"
    SEINEN = 42
    JOSEI = 43
    AWARD_WINNING = 46  # New
    GOURMET = 47  # New
    WORK_LIFE = 48  # New
    EROTICA = 49  # New
    

    @staticmethod
    def get_genre_from_string(genre: str):
        genre = genre.upper().strip().replace(' ', '_').replace('-', '_')
        return Genre[genre]

    @staticmethod
    def encode_to_string(genres: tuple) -> str:
        return ','.join([str(genre.value) for genre in genres])

    @staticmethod
    def decode_from_string(genres: str) -> tuple:
        return tuple(Genre(int(genre)) for genre in genres.split(',') if genre.isnumeric())
