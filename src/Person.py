import Logger
from lxml.html import HtmlElement
from Utility import sanitize_string


class Person:
    def __init__(self, character_tree: HtmlElement):
        self.id = None
        self.name = None
        self.favourites = None
        self._extract_information(character_tree)
        Logger.get_logger('MetaMAL.Person').debug(f'Person analyzed: {str(self)}')

    def __str__(self) -> str:
        return f'[{self.id}] {self.name} → Favourites: {self.favourites}'

    def _extract_information(self, tree: HtmlElement):
        self._extract_name(tree)
        self._extract_favourites(tree)
        self._extract_id(tree)

    def _extract_name(self, tree: HtmlElement):
        name = tree.find_class('name')[0]
        self.name = sanitize_string(name.text_content())
        comma_index = self.name.find(', ')
        if comma_index > 0:
            self.name = f'{self.name[comma_index + 2:]} {self.name[:comma_index]}'

    def _extract_favourites(self, tree: HtmlElement):
        favourites = tree.find_class('users')[0]
        self.favourites = int(sanitize_string(
            favourites.text_content()).replace(',', '').replace('user', '').replace('s', ''))

    def _extract_id(self, tree: HtmlElement):
        url = tree.find_class('thumb')[0].attrib['href']
        self.id = int(url.split('/')[4])
