import Backup
import DbManager
import json
import os
import os.path
import SpreadsheetExporter
from collections import OrderedDict
from Logger import get_logger
from requests import Session
from sqlite3 import Connection, OperationalError
from Utility import Season


ROOT_DIR = os.path.abspath('..')
JSON_DATA_DIR = os.path.abspath(os.path.join('..', 'data'))
categories = ('tv_new', 'tv_continuing', 'ona', 'ova', 'movie', 'special')
score_selector = lambda anime: anime.score
members_selector = lambda anime: anime.members
_logger = get_logger('MetaMAL.DataAnalyzer')


def compute_website_data(season: Season, year: int, ignore_general_data = False, skip_backup = False) -> tuple:
    # - Top 15 best score anime
    # - Top 15 most popular anime
    # - Most popular genres
    # - Top 15 best character
    # - Top 15 best seiyuu
    global _logger, score_selector, members_selector, JSON_DATA_DIR
    session = Session()
    _logger.debug(f'Computing data for {season.value} {year}.')
    DbManager.remove_duplicates(session, year, season)
    connection = DbManager.get_new_connection(True)
    anime_per_category = _get_every_anime_for_each_category(connection, year, season)
    _logger.debug('Extracted anime from database.')
    top_anime_score = _get_anime_with_best_value(15, anime_per_category, score_selector, connection)
    _logger.debug('Computed anime with best score.')
    top_anime_popularity = _get_anime_with_best_value(15, anime_per_category, members_selector, connection)
    _logger.debug('Computed most popular anime.')
    top_genres = _get_most_popular_genres(15, anime_per_category, connection)
    _logger.debug('Computed most popular genres.')
    top_characters = _get_most_popular_characters_or_seiyuu(True, 15, year, season, connection, session)
    _logger.debug('Computed most popular characters.')
    top_seiyuu = _get_most_popular_characters_or_seiyuu(False, 15, year, season, connection, session)
    session.close()
    _logger.debug('Computed most popular seiyuu.')
    connection.close()

    connection = DbManager.get_new_connection()
    DbManager.compress_database(connection)
    connection.close()

    filename = os.path.join(JSON_DATA_DIR, f'{year}-{season.value}.json')
    _export_data(top_anime_score, top_anime_popularity, top_genres, top_characters, top_seiyuu, filename, False, season, year)

    compute_full_website_data(season, year)

    if not skip_backup:
        Backup.backup_database()

    if not ignore_general_data:
        previous_season, previous_year, last_day_of_previous_season = export_general_data()
        return previous_season, previous_year, last_day_of_previous_season
    else:
        return None,


def compute_full_website_data(season: Season, year: int):
    global _logger, score_selector, members_selector, JSON_DATA_DIR
    session = Session()
    _logger.debug(f'Computing extensive data for {season.value} {year}.')
    connection = DbManager.get_new_connection(True)
    anime_per_category = _get_every_anime_for_each_category(connection, year, season)
    _logger.debug('Extracted anime from database.')
    top_anime_score = _get_anime_with_best_value(200, anime_per_category, score_selector, connection)
    _logger.debug('Computed anime with best score (extensive).')
    top_anime_popularity = _get_anime_with_best_value(200, anime_per_category, members_selector, connection)
    _logger.debug('Computed most popular anime (extensive).')
    top_genres = _get_most_popular_genres(30, anime_per_category, connection)
    _logger.debug('Computed most popular genres (extensive).')
    top_characters = _get_most_popular_characters_or_seiyuu(True, 50, year, season, connection, session)
    _logger.debug('Computed most popular characters (extensive).')
    top_seiyuu = _get_most_popular_characters_or_seiyuu(False, 50, year, season, connection, session)
    _logger.debug('Computed most popular seiyuu (extensive).')
    connection.close()

    filename = os.path.join(JSON_DATA_DIR, f'{year}-{season.value}-full.json')
    _export_data(top_anime_score, top_anime_popularity, top_genres, top_characters, top_seiyuu, filename, True, season, year)
    SpreadsheetExporter.generate_spreadsheet(season.value, year)

    check_missing_pictures(session)
    session.close()


def _get_every_anime_for_each_category(connection: Connection, year: int, season: Season) -> dict:
    global categories
    anime_per_category = {}
    for category in categories:
        anime_per_category[category] = _get_every_anime_for_season(connection, year, season, category)
    return anime_per_category


def _get_every_anime_for_season(connection: Connection, year: int, season: Season, category: str) -> tuple:
    anime_per_date = OrderedDict()
    data_per_anime = {}
    anime_list = DbManager.get_anime_per_season(connection, year, season, category)

    for anime in anime_list:
        data = anime_per_date.get(anime.timestamp, [])
        data.append(anime)
        anime_per_date[anime.timestamp] = data

        data = data_per_anime.get(anime.id, [])
        data.append(anime)
        data_per_anime[anime.id] = data
    return anime_per_date, data_per_anime


def _get_anime_with_best_value(amount: int, anime_per_category: dict, value_selector, connection: Connection) -> dict:
    global categories
    top_anime = {}
    for category in categories:
        top_anime[category] = _get_anime_with_best_value_for_category(amount, anime_per_category[category], value_selector, connection)
    return top_anime


def _get_anime_with_best_value_for_category(amount: int, data: tuple, value_selector, connection: Connection) -> dict:
    anime_per_date, data_per_anime = data
    best_anime_per_day = []
    anime_ids_to_keep = set()
    for date, anime in anime_per_date.items():
        current_best_anime_per_day = _get_anime_with_best_value_per_day(amount, anime[:], value_selector, connection)
        anime_ids_to_keep = anime_ids_to_keep.union([a['Id'] for a in current_best_anime_per_day])
        best_anime_per_day.append([date, current_best_anime_per_day])
    _check_anime_presence_consistency(best_anime_per_day, anime_ids_to_keep, data_per_anime, value_selector, amount, connection)
    return {couple[0]: couple[1] for couple in best_anime_per_day}


def _get_anime_with_best_value_per_day(amount: int, anime: list, value_selector, connection: Connection) -> list:
    anime = [a for a in anime if value_selector(a) is not None]
    anime = sorted(anime, key = lambda element: value_selector(element), reverse = True)
    anime = anime[:amount]
    [a.load_title(connection) for a in anime]
    return [a.__dict__() for a in anime]


def _check_anime_presence_consistency(best_anime_per_day: list, anime_ids_to_keep: set, data_per_anime: dict, value_selector, amount: int, connection: Connection):
    if len(best_anime_per_day) == 0:
        return

    for date, anime_list in best_anime_per_day:
        ids = set([a['Id'] for a in anime_list])
        missing_ids = anime_ids_to_keep.difference(ids)
        for anime_id in missing_ids:
            anime = data_per_anime.get(anime_id, None)
            if anime is None:
                continue
            anime = tuple(filter(lambda a: a.timestamp == date, anime))
            if len(anime) > 0 and value_selector(anime[0]) is not None:
                anime[0].load_title(connection)
                anime_list.append(anime[0].__dict__())

    last_date, last_anime_list = best_anime_per_day[-1]
    ids = set([a['Id'] for a in last_anime_list[:amount]])
    for i in range(len(best_anime_per_day)):
        date, anime = best_anime_per_day[i]
        best_anime_per_day[i][1] = list(filter(lambda a: a['Id'] in ids, anime))


def _get_most_popular_genres(amount: int, anime_per_category: dict, connection: Connection) -> list:
    global categories
    popularity_per_genre = {}
    for category in categories:
        anime_per_date, data_per_anime = anime_per_category[category]
        if len(anime_per_date) == 0:
            continue
        _get_most_popular_genres_per_category(anime_per_date, popularity_per_genre, connection)

    elements = list(popularity_per_genre.items())
    return sorted(elements, key = lambda couple: couple[1][0], reverse = True)[:amount]


def _get_most_popular_genres_per_category(anime_per_date: OrderedDict, popularity_per_genre: dict, connection: Connection):
    date, anime_list = get_most_recent_element(anime_per_date)
    for anime in anime_list:
        genres = DbManager.get_genres_per_anime(connection, anime.id)
        for genre in genres:
            members = popularity_per_genre.get(genre.name, (0, 0))
            popularity_per_genre[genre.name] = members[0] + anime.members, members[1] + 1


def get_most_recent_element(dictionary: OrderedDict) -> tuple:
    if len(dictionary) > 0:
        last = dictionary.popitem()
        dictionary[last[0]] = last[1]
        return last
    return tuple()


def _get_most_popular_characters_or_seiyuu(characters: bool, amount: int, year: int, season: Season, connection: Connection, session: Session) -> list:
    if characters:
        entities = DbManager.get_characters_per_season(connection, year, season)
        # Excluding One Piece and Naruto
        excluded_ids = {13, 17, 40, 61, 62, 64, 85, 305, 309, 723, 724, 1555, 1662, 2007, 3879, 3889, 4896, 4898, 5627, 13767, 117517, 117791, 128188}
        entities = list(filter(lambda c: c.id not in excluded_ids, entities))
    else:
        entities = DbManager.get_seiyuu_per_season(connection, year, season)

    if len(entities) == 0:
        return []

    entities = sorted(entities, key = lambda e: e.timestamp)
    entities_per_date = OrderedDict()
    for entity in entities:
        entries = entities_per_date.get(entity.timestamp, [])
        entries.append(entity)
        entities_per_date[entity.timestamp] = entries
    return _get_entities_with_higher_delta(entities_per_date, amount, connection, session)


def _get_entities_with_higher_delta(entities_per_date: OrderedDict, amount: int, connection: Connection, session: Session) -> list:
    entities_per_delta = {}
    last_date, last_entities = get_most_recent_element(entities_per_date)
    for entity in last_entities:
        i = 0
        first_date, first_entities = tuple(entities_per_date.items())[i]
        old_entity = tuple(filter(lambda e: e.id == entity.id, first_entities))
        while len(old_entity) < 1:
            i += 1
            first_date, first_entities = tuple(entities_per_date.items())[i]
            old_entity = tuple(filter(lambda e: e.id == entity.id, first_entities))
        delta = abs(entity.members - old_entity[0].members)
        entries = entities_per_delta.get(delta, [])
        entries.append(entity)
        entities_per_delta[delta] = entries

    delta_entities = list(entities_per_delta.items())
    delta_entities = sorted(delta_entities, key = lambda e: e[0])
    result = []
    count = 0
    while len(delta_entities) > 0 and count <= amount:
        element = delta_entities.pop()  # (1, [,])
        [e.load_name(connection) or e.save_picture(session) for e in element[1]]
        element = (element[0], [e.__dict__() for e in element[1]])
        result.append(element)
        count += len(element[1])
    return result


def _export_data(top_anime_score: dict, top_anime_popularity: dict, top_genres: list, top_characters: list, top_seiyuu: list, filename: str, full_data: bool, season: Season,
                 year: int):
    global _logger, JSON_DATA_DIR
    try:
        data = {
            'TopAnimeScore': top_anime_score,
            'TopAnimePopularity': top_anime_popularity,
            'TopGenres': top_genres,
            'TopCharacters': top_characters,
            'TopSeiyuu': top_seiyuu,
            'Season': season.value,
            'Year': year
        }

        os.makedirs(JSON_DATA_DIR, exist_ok = True)
        with open(filename, 'w', encoding = 'utf-8') as file:
            json.dump(data, file)
        _logger.info(f'Data successfully exported in {filename}.')
    except Exception as exception:
        _logger.exception('Could not export anime data.', exception)


def export_general_data() -> tuple:
    global JSON_DATA_DIR, _logger
    try:
        season = Season.SUMMER
        year = 2019
        data = {'seasons': {}, 'first season': 'SUMMER', 'first year': 2019}
        connection = DbManager.get_new_connection(True)

        while _write_first_and_last_day_per_season(season, year, data, connection):
            season, year = Season.get_next_season(None, season, year)
        connection.close()

        os.makedirs(JSON_DATA_DIR, exist_ok = True)
        with open(os.path.join(JSON_DATA_DIR, 'seasons.json'), 'w', encoding = 'utf-8') as file:
            json.dump(data, file)

        _logger.info(f'General season data successfully exported in seasons.json.')
        previous_season, previous_year = Season.get_previous_season(None, season, year)
        previous_season, previous_year = Season.get_previous_season(None, previous_season, previous_year)
        last_day_of_previous_season = data['seasons'][previous_year][f'{previous_season.name} end']
        return previous_season, previous_year, last_day_of_previous_season
    except Exception as exception:
        _logger.exception('Could not export seasons data.', exception)


def _write_first_and_last_day_per_season(season: Season, year: int, data: dict, connection: Connection) -> bool:
    global _logger
    try:
        start = DbManager.get_first_entry_per_season(connection, year, season)
        end = DbManager.get_last_entry_per_season(connection, year, season)
        target = data['seasons'].get(year, {})
        target[f'{season.name} start'] = start
        target[f'{season.name} end'] = end
        data['seasons'][year] = target
        data['last season'] = season.name
        data['last year'] = year
        _logger.debug(f'{season.name} {year}: {start} ---> {end}')
        return True
    except OperationalError as exception:
        if not exception.args[0].startswith('no such table'):
            _logger.exception('Could not read anime data.', exception)
        return False
    except Exception as exception:
        _logger.exception('Unexpected error while reading anime data.', exception)
        return False


def check_missing_pictures(session: Session):
    global ROOT_DIR, _logger
    try:
        characters_ids, people_ids = _get_entities_ids()

        picture_dir = os.path.join(ROOT_DIR, 'pictures', 'characters')
        files = set([int(os.path.basename(f).split('.')[0]) for f in os.listdir(picture_dir)
                     if os.path.isfile(os.path.join(picture_dir, f))])
        for entity_id in characters_ids:
            if entity_id not in files:
                DbManager.EntityData((entity_id, -1, ''), True).save_picture(session)
                _logger.info(f'Found missing character picture with ID {entity_id}.')

        picture_dir = os.path.join(ROOT_DIR, 'pictures', 'seiyuu')
        files = set([int(os.path.basename(f).split('.')[0]) for f in os.listdir(picture_dir)
                     if os.path.isfile(os.path.join(picture_dir, f))])
        for entity_id in people_ids:
            if entity_id not in files:
                DbManager.EntityData((entity_id, -1, ''), False).save_picture(session)
                _logger.info(f'Found missing seiyuu picture with ID {entity_id}.')

        _logger.debug(f'{str(len(characters_ids) + len(people_ids))} missing picture(s) downloaded.')
    except Exception as error:
        _logger.exception('Could not check missing pictures.', error)


def _get_entities_ids() -> tuple:
    global JSON_DATA_DIR, _logger
    data_files = [f for f in os.listdir(JSON_DATA_DIR)
                  if os.path.isfile(os.path.join(JSON_DATA_DIR, f))
                  and os.path.basename(os.path.join(JSON_DATA_DIR, f)).startswith('20')
                  and os.path.basename(os.path.join(JSON_DATA_DIR, f)).endswith('.json')]
    _logger.debug(f'Found {len(data_files)} seasonal data files.')

    characters_ids = set()
    people_ids = set()
    for json_file in data_files:
        with open(os.path.join(JSON_DATA_DIR, json_file)) as file:
            data = json.load(file)
        _logger.debug(f'File {json_file} loaded successfully.')

        characters = data['TopCharacters']
        characters = [[c['Id'] for c in x[1]] for x in characters]
        for id_list in characters:
            for character_id in id_list:
                characters_ids.add(character_id)

        people = data['TopSeiyuu']
        people = [[p['Id'] for p in x[1]] for x in people]
        for id_list in people:
            for person_id in id_list:
                people_ids.add(person_id)
    _logger.debug(f'Found {len(characters_ids)} characters and {len(people_ids)} peoples.')
    return characters_ids, people_ids


if __name__ == "__main__":
    temp_session = Session()
    current_season, current_year = Season.get_current_season(temp_session)
    temp_session.close()
    compute_website_data(current_season, current_year)
